package com.unb.smartfridge.service.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import java.util.Date;

import static androidx.room.ForeignKey.CASCADE;

@Entity( foreignKeys = {
    @ForeignKey(entity = Fridge.class,
            parentColumns = "fridgeId",
            childColumns = "fridgeId",
            onDelete = CASCADE),
})
public class ActivityItem {

    @PrimaryKey(autoGenerate = true)
    private int activityId;

    @ColumnInfo(index = true)
    private int fridgeId;
    private String title;
    private String description;

    @TypeConverters({Converter.class})
    private Date timestamp;

    public ActivityItem(int fridgeId, String title, String description, Date timestamp) {
        this.fridgeId = fridgeId;
        this.title = title;
        this.description = description;
        this.timestamp = timestamp;
    }

    public void setActivityId(int activityId) {
        this.activityId = activityId;
    }

    public int getActivityId() {
        return activityId;
    }

    public int getFridgeId() {
        return fridgeId;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public Date getTimestamp() {
        return timestamp;
    }
}

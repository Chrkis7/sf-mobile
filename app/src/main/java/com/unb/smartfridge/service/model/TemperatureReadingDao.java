package com.unb.smartfridge.service.model;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.TypeConverters;

import java.util.Date;
import java.util.List;

@Dao
@TypeConverters(Converter.class)
public interface TemperatureReadingDao {

    @Query("SELECT * FROM TemperatureReading")
    List<TemperatureReading> getAllTemperatureReadings();

    @Query("SELECT * FROM TemperatureReading WHERE readingId = :readingId")
    TemperatureReading getTemperatureReadingById(int readingId);

    @Query("SELECT * FROM TemperatureReading WHERE timestamp BETWEEN :from AND :to")
    List<TemperatureReading> getTemperatureReadingsBetweenDates(Date from, Date to);

    @Query("SELECT * FROM TemperatureReading ORDER BY readingId DESC LIMIT 1")
    TemperatureReading getLatestingTemperatureReading();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insertTemperatureReading(TemperatureReading temperatureReading);
}

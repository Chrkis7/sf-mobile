package com.unb.smartfridge.service.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

import static androidx.room.ForeignKey.CASCADE;

@Entity(foreignKeys = @ForeignKey(entity = ItemInfo.class,
        parentColumns = "itemUpc",
        childColumns = "itemUpc",
        onDelete = CASCADE))
public class ItemPreference {

    @PrimaryKey(autoGenerate = true)
    private int preferenceId;

    @ColumnInfo(index = true)
    private float itemUpc;

    private String itemAlias;

    @ColumnInfo(typeAffinity = ColumnInfo.BLOB)
    byte[] itemImage;

    public ItemPreference(int preferenceId, float itemUpc, String itemAlias, byte[] itemImage) {
        this.preferenceId = preferenceId;
        this.itemUpc = itemUpc;
        this.itemAlias = itemAlias;
        this.itemImage = itemImage;
    }

    public int getPreferenceId() {
        return preferenceId;
    }

    public float getItemUpc() {
        return itemUpc;
    }

    public String getItemAlias() {
        return itemAlias;
    }

    public byte[] getItemImage() {
        return itemImage;
    }
}

package com.unb.smartfridge.service.background;

import android.app.Application;
import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import com.unb.smartfridge.R;
import com.unb.smartfridge.SmartFridgeApplication;
import com.unb.smartfridge.service.model.ActivityItem;
import com.unb.smartfridge.service.repository.FridgeRepository;
import com.unb.smartfridge.service.repository.ItemInfoRepository;
import com.unb.smartfridge.service.repository.ItemRepository;

import java.util.Date;
import java.util.List;

public class ExpiryWorker extends Worker {

    private final String TAG = "ExpiryWorker";

    private ItemRepository itemRepo;
    private ItemInfoRepository itemInfoRepo;
    private FridgeRepository fridgeRepo;

    public ExpiryWorker(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
        SmartFridgeApplication app = SmartFridgeApplication.getInstance();
        this.itemRepo = new ItemRepository(app);
        this.itemInfoRepo = new ItemInfoRepository(app);
        this.fridgeRepo = new FridgeRepository(app);
    }

    @NonNull
    @Override
    public Result doWork() {
        List<Integer> updatedItemIds = itemRepo.updateExpiredItems();

        if (updatedItemIds == null || updatedItemIds.isEmpty()) {
            Log.d(TAG, "No new items have expired.");
            return Result.failure();
        } else {
            Log.d(TAG, "The items with the following IDs have expired: " + updatedItemIds.toString());
        }

        //TODO Send notification to user about the expired items

        for (Integer itemId: updatedItemIds) {
            ActivityItem expiryActivity = createNewExpiryActivity(itemId);
            fridgeRepo.insertActivity(expiryActivity);
        }

        return Result.success();
    }

    private ActivityItem createNewExpiryActivity(int itemId) {
        String itemName  = itemInfoRepo.getItemInfoById(itemRepo.getItemById(itemId).getItemUpc()).getName();
        String activityDescription = "'" + itemName + "' " + getApplicationContext()
                .getResources()
                .getString(R.string.activity_item_description_item_expired);
        String activityTitle = getApplicationContext()
                .getResources()
                .getString(R.string.activity_item_title_item_expired);
        ActivityItem expiryActivity = new ActivityItem(1, activityTitle, activityDescription, new Date());

        return expiryActivity;
    }
}

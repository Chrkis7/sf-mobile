package com.unb.smartfridge.viewmodel;

import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.unb.smartfridge.service.model.ActivityItem;
import com.unb.smartfridge.service.repository.FridgeRepository;

import java.util.List;

public class ActivityViewModel extends AndroidViewModel {

    private final String TAG = "ActivityViewModel";
    private FridgeRepository repository;

    public ActivityViewModel(@NonNull Application application) {
        super(application);
        repository = new FridgeRepository(application);
    }

    public LiveData<List<ActivityItem>> getActivitiesItem(int maxActivities) {
        Log.d(TAG, "Retrieving ActivityItems from the ViewModel.");
        return repository.getRecentActivityItems(maxActivities);
    }
}

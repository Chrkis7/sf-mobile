package com.unb.smartfridge.view;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.unb.smartfridge.R;
import com.unb.smartfridge.databinding.FragmentOverviewBinding;
import com.unb.smartfridge.viewmodel.OverviewViewModel;

public class OverviewFragment extends Fragment {

    private final String TAG = "OverviewFragment";

    private OverviewViewModel overviewViewModel;
    private FragmentOverviewBinding overviewFragmentBinding;

    public OverviewFragment() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        overviewViewModel = new ViewModelProvider(this).get(OverviewViewModel.class);
        getActivity().setTitle(overviewViewModel.getFridgeName());

        // Inflate the layout with Data Binding
        overviewFragmentBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_overview, container, false);
        overviewFragmentBinding.setViewmodel(overviewViewModel);
        overviewFragmentBinding.overviewFeaturedItemLayout.setViewmodel(overviewViewModel);
        View view = overviewFragmentBinding.getRoot();

        return view;
    }
}

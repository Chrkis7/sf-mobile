package com.unb.smartfridge.view;


import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.unb.smartfridge.R;
import com.unb.smartfridge.service.model.ActivityItem;
import com.unb.smartfridge.view.adapters.ActivityAdapter;
import com.unb.smartfridge.viewmodel.ActivityViewModel;

import java.util.List;

public class ActivityFragment extends Fragment {

    private final String FRAGMENT_NAME = "Activity";
    private final String TAG = "ActivityFragment";
    private final int MAX_ACTIVITY_ITEMS = 15;

    private ActivityViewModel activityViewModel;

    public ActivityFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getActivity().setTitle(FRAGMENT_NAME);
        View view = inflater.inflate(R.layout.fragment_activity, container, false);

        RecyclerView recyclerView = view.findViewById(R.id.activity_recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(view.getContext()));
        recyclerView.setHasFixedSize(true);

        final ActivityAdapter adapter = new ActivityAdapter();
        recyclerView.setAdapter(adapter);

        activityViewModel = new ViewModelProvider(this).get(ActivityViewModel.class);
        recyclerView.addItemDecoration(new DividerItemDecoration(recyclerView.getContext(), DividerItemDecoration.VERTICAL));

        activityViewModel.getActivitiesItem(MAX_ACTIVITY_ITEMS).observe(this, new Observer<List<ActivityItem>>() {
            @Override
            public void onChanged(List<ActivityItem> activities) {
                adapter.setActivities(activities);
            }
        });

        return view;
    }
}
